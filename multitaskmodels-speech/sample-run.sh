#!/bin/bash
#$ -M aanastas@nd.edu
#$ -m abe
#$ -r n
#$ -q *@@nlp-gpu
#$ -N mb-sp-m
#$ -l gpu_card=1

modeltype=triangle
# Paths to directories for outputs
modeldir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_models/mboshi-speech
outputdir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_outputs/mboshi-speech
mkdir -p $modeldir
mkdir -p $outputdir

# Path to data
data=/afs/crc.nd.edu/group/nlp/data/mboshi/mboshi-french-parallel-corpus-master/full_corpus_newsplit/train/
speechdata=$data/plp/
mbdata=$data/mbclsp/
frdata=$data/frclsp/

# List of training and dev files
trainfiles=$data/train_files.txt
devfiles=$data/dev_files.txt

# File extensions
speechend=.plp
text1end=.mb.cleaned.split
text2end=.fr.cleaned.split

# Path to test data
testdata=/afs/crc.nd.edu/group/nlp/data/mboshi/mboshi-french-parallel-corpus-master/full_corpus_newsplit/dev
testfiles=$testdata/test_files.txt
testspeechdata=$testdata/plp/

# Logging parameters
l1=mbs
l2=mbc
l3=frc
size=512
layers=1
dropout=0.2
desc=$size-$layers-$layers-$dropout
log=log.txt

# Training
./train_multitaskmodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $mbdata --text_dir2 $frdata --out $modeldir/$modeltype-$desc-$l1$l2$l3 --layers $layers --size $size --dropout $dropout --epochs 500 --batchsize 4 --speech_end $speechend --text1_end $text1nend --text2_end $text2end &> $log

# Testing
modelin=$modeldir/$modeltype-$desc-$l1$l2$l3.best.params
output=$outputdir/$l1$l2$l3.test.$modeltype.$desc.best
./examples/train_multitaskmodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $mbdata --text_dir2 $frdata --in $modelin --test $testfiles --test_speech_dir $testspeechdata  --layers $layers --size $size --speech_end $speechend --text1_end $text1nend --text2_end $text2end> $output

# Force decode attentions
attentiondir=/afs/crc.nd.edu/group/nlp/03/aanastas/dynet_attentions/mboshi-speech
mkdir -p $attentiondir
testtxt1=$testdata/mbclsp/
testtxt2=$testdata/frclsp/
output=$attentiondir/$l1$l2$l3.test.$modeltype.$desc.best
./examples/train_multitaskmodels-speech $modeltype $trainfiles $devfiles --speech_dir $speechdata --text_dir1 $mbdata --text_dir2 $frdata --in $modelin --test $testfiles --test_speech_dir $testspeechdata --test_text_dir1 $testtxt1 --test_text_dir2 $testtxt2 --layers $layers --size $size --speech_end $speechend --text1_end $text1nend --text2_end $text2end --attention > $output


