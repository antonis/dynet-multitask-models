# Neural Sequence-to-sequence multitask models

Writen in C++. You need to have [DyNet](http://dynet.readthedocs.io/en/latest/) installed.

Sample run script is `multitaskmodels-speech/sample_run.sh`.

The models implemented are:

* `simpleunitask` : simple, single task enc-dec with attention

* `simplemultitask` : simple multi task: two decoders, with two attention mechanisms over the same encoded source

* `cascade` : two decoders with the second decoder attending over the last states of the first decoder
	(as in the reconstruction model of Tu et al. 2017)

* `cascade_reg` : same as cascade, except with a regularizer to force the two attentions to be inverses of each other.

* `triangle` : two decoders, with the second decoder attending over both the encoded source
	and the sequences of the last states of the first decoder.
	(as introduced in Anastasopoulos and Chiang 2018)

* `triangle_reg` : same as triangle, with additional regularizer to apply transitivity on the attention matrices
	(as described in Anastasopoulos and Chiang 2018)

Substitute the dynet/dict.[h,cc] files with the ones provided, in order for the code to compile -- they just add
a function for reading speech features.

For more information, take a look at our paper:

Antonis Anastasopoulos and David Chiang. (NAACL 2018) [Tied Multitask Learning for Neural Speech Translation](https://arxiv.org/abs/1802.06655).
to appear in NAACL 2018.

bibtex information:

	@inproceedings{anastasopoulos+chiang:naacl2018,
        author = "Anastasopoulos, Antonios and Chiang, David",
        title = "Tied Multitask Learning for Neural Speech Translation",
        year = "2018",
        note = "To appear",
        booktitle = "Proc. NAACL HLT"
    }

